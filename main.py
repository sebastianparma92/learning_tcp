import socket
import struct
import dictionary


class EthernetHeader:

    def __init__(self):
        self._get_raw_data = None

    @property
    def get_raw_data(self):
        return self._get_raw_data

    @get_raw_data.setter
    def get_raw_data(self, raw_data):
        self._get_raw_data = raw_data

    @property
    def mac_destination(self):
        return struct.unpack('! 6s', self.get_raw_data[:6])[0].hex()

    @property
    def mac_source(self):
        return struct.unpack('! 6s', self.get_raw_data[6:12])[0].hex()

    @property
    def ethertype(self):
        return dictionary.ethertype[struct.unpack('! H', self.get_raw_data[12:14])[0]]

    @property
    def payload(self):
        return self.get_raw_data[14:]

class IpV4Header:

    """IP v4 header based on RFC 0791"""

    def __init__(self):
        self._get_data = None

    @property
    def get_data(self):
        return self._get_data

    @get_data.setter
    def get_data(self, data):
        self._get_data = data

    @property
    def version_and_length(self):
        return self.get_data[0]

    @property
    def version(self):
        return self.version_and_length >> 4

    @property
    def header_length(self):
        return (self.version_and_length & 0x0F) * 4

    @property
    def type_of_service(self):
        return self.get_data[1]

    @property
    def total_length(self):
        return struct.unpack('! H', self.get_data[2:4])[0]

    @property
    def id(self):
        return struct.unpack('! H', self.get_data[4:6])[0]

    @property
    def flags_offset(self):
        return struct.unpack('! H', self.get_data[6:8])[0]

    @property
    def flags(self):
        return self.flags_offset >> 13

    @property
    def offset(self):
        return self.flags_offset & 0x1FFF

    @property
    def ttl(self):
        return self.get_data[8]

    @property
    def protocol(self):
        return dictionary.ipv4_protocol[self.get_data[9]]

    @property
    def header_crc(self):
        return struct.unpack("! H", self.get_data[10:12])

    @property
    def ip_src(self):
        return self.get_ip(struct.unpack("! 4s", self.get_data[12:16])[0])

    @property
    def ip_target(self):
        return self.get_ip(struct.unpack("! 4s", self.get_data[16:20])[0])

    @property
    def payload(self):
        return self.get_data[20:]

    def get_ip(self, addr):
        return '.'.join(map(str, addr))



class UDPHeader:

    def __init__(self):
        self._get_data = None

    @property
    def get_data(self):
        return self._get_data

    @get_data.setter
    def get_data(self, data):
        self._get_data = data

    @property
    def source_port(self):
        return struct.unpack("! H", self.get_data[0:2])[0]

    @property
    def destination_port(self):
        return struct.unpack("! H", self.get_data[2:4])[0]

    @property
    def total_length(self):
        return struct.unpack("! H", self.get_data[4:6])[0]

    @property
    def crc(self):
        return struct.unpack("! H", self.get_data[6:8])[0]

    @property
    def payload(self):
        return self.get_data[8:]





if __name__ == "__main__":
    s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
    ethernet_header = EthernetHeader()
    ipv4_header = IpV4Header()
    udp_header = UDPHeader()
    while True:
        raw_data, addr = s.recvfrom(65535)
        ethernet_header.get_raw_data = raw_data
        if ethernet_header.ethertype == "IPv4":
            ipv4_header.get_data = ethernet_header.payload
            if ipv4_header.protocol == 'UDP':
                udp_header.get_data = ipv4_header.payload
                print("Ethernet header: "
                      "MAC destination: {}, "
                      "MAC source: {}, "
                      "ethertype {}, "
                      "payload_len {}"
                      .format(ethernet_header.mac_destination,
                              ethernet_header.mac_source,
                              ethernet_header.ethertype,
                              len(ethernet_header.payload)))
                print("Ipv4 version: {}, "
                      "header length: {}, "
                      "type of service {}, "
                      "total length: {}, "
                      "id: {}, "
                      "flags {}, "
                      "offset {}, "
                      "ttl: {}, "
                      "protocol: {}, "
                      "ip src: {}, "
                      "ip target: {}"
                      .format(ipv4_header.version,
                              ipv4_header.header_length,
                              ipv4_header.type_of_service,
                              ipv4_header.total_length,
                              ipv4_header.id,
                              ipv4_header.flags,
                              ipv4_header.offset,
                              ipv4_header.ttl,
                              ipv4_header.protocol,
                              ipv4_header.ip_src,
                              ipv4_header.ip_target))
                print("Source port: {}, "
                      "Destination port: {}, "
                      "Length: {}"
                      .format(udp_header.source_port,
                              udp_header.destination_port,
                              udp_header.total_length))
                print("UDP Datagram: {}".format(udp_header.payload))

